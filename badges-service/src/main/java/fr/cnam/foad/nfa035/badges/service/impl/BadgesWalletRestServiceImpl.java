package fr.cnam.foad.nfa035.badges.service.impl;

public class BadgesWalletRestServiceImpl {
    public void deleteBadge(){
    }

            /**
             * Lecture du Wallet => R
             *
             * @return ResponseEntity la réponse REST toujours
             */
        @Operation(summary = "Récupère le métadonnées du Wallet",
                description = "Récupère le métadonnées du portefeuille, c'est à dire l'index des badges qui s'y trouve"

                @Tag(name = "getMetadata")
            @GetMapping("/metas")
                    ResponseEntity<Set<DigitalBadge>> getMetadata();



                return ResponseEntity
                .ok()
                .body(jsonBadgeDao.getWalletMetadata())

}
